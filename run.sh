#!/bin/bash

set -eu

cd /data/project/vendor/tool-vendor
php bin/fetch.php
php vendor/bin/semver-checker upgrade-lock mediawiki-vendor.lock > ~/public_html/safe.txt
php vendor/bin/semver-checker upgrade-lock mediawiki-vendor.lock --unsafe > ~/public_html/unsafe.txt
