<?php
/**
 * Copyright (C) 2018 Kunal Mehta <legoktm@member.fsf.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

namespace Toolforge\Vendor;

use Requests;

class LockFetcher {

	private $url;

	public function __construct() {
		$this->url = 'https://phabricator.wikimedia.org/diffusion/MWVD/browse/master/composer.lock?view=raw';
	}

	public function fetch() {
		$req = Requests::get( $this->url );
		$req->throw_for_status();

		$data = $req->body;
		file_put_contents( 'mediawiki-vendor.lock', $data );
		echo "Downloaded mediawiki/vendor's composer.lock\n";
	}

}